const axios = require('axios')

const weatherUrl = 'https://goweather.herokuapp.com/weather/'

class Weather {
    /**
     * Describe attributes: temperature, wind, date
     */
    constructor() {
        this.temperature = ''
        this.wind = ''
        this.date = new Date()
        this.forecast = ''
    }

    async setWeather(cityName) {
        await axios.get(weatherUrl + cityName).then((response) => {
            this.temperature = response.data.temperature
            this.wind = response.data.wind
        }).catch((error) => { return error.message })
    }

    // Добавить к классу погоды атрибут forecast и метод setForecast 
    // (в этом методе использовать вызов того же API, с помощью которого вытягивалась погода, но взять из JSON объекта другие поля). 
    // Также добавить соответствующий метод в класс города.

    async setForecast(cityName) {
        await axios.get(weatherUrl + cityName).then((response) => {
            this.forecast = response.data.forecast
        }).catch((error) => { return error.message })
    }
}

module.exports = { Weather }