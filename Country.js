// Создать класс Country. У сущности Country должны быть атрибуты: имя, массив городов, которые входят в страну. 
// Создать методы добавления города в страну, удаления города по имени (использовать методы findIndex, splice объекта Array).
const { City } = require('./City')

class Country {

    constructor(name) {
        this.name = name
        this.cityList = []
    }

    addCity(cityName) {
        this.cityList.push(cityName)
    }

    deleteCity(cityName) {
        this.cityList.splice(this.cityList.indexOf(cityName))
    }
}

module.exports = { Country }