const { City } = require('./City')
const { Capital } = require('./Capital')
const { Country } = require('./Country')

let city1 = new City('Kharkiv')
let city2 = new City('Lviv')

city1.setWeather().then(() => { console.log(city1) }).catch((error) => { console.log(error) })
city1.setForecast().then(() => { console.log(city1.weather.forecast) }).catch((error) => { console.log(error) })
city2.setWeather().then(() => { console.log(city2) }).catch((error) => { console.log(error) })
city2.setForecast().then(() => { console.log(city2.weather.forecast) }).catch((error) => { console.log(error) })

let capital = new Capital('Kyiv')
capital.setAirport('Boryspil')
capital.setWeather().then(() => { console.log(capital) }).catch((error) => { console.log(error) })



let country1 = new Country('Ukraine')
country1.addCity('Kyiv')
console.log(country1)
country1.deleteCity('Kyiv')
console.log(country1)

function parseNumber(n) {
    return Number(n.replace(/[^0-9\.-]+/g, ""))
}

function sortAsc(a, b) {
    if (a.cityWeather > b.cityWeather)
        return 1
    if (a.cityWeather < b.cityWeather)
        return -1
    return 0
}

// Создать страну и 10 городов в ней, для каждого города достать погоду.Отсортировать города по спаданию температуры.

let country2 = new Country('USA')
const cities = ['Boston', 'Jacksonville', 'Chicago', 'Seattle', 'Denver',
    'Philadelphia', 'Columbus', 'Charlotte', 'Dallas', 'Indianapolis'
]

cities.forEach(city => country2.addCity(city))

listTemp = []


country2.cityList.forEach(city => {
    let a = new City(city)
    a.setWeather()
        .then(() => {
            let cityTemp = {
                cityName: a.name,
                cityWeather: parseNumber(a.weather.temperature)
            }
            listTemp.push(cityTemp)
            listTemp.sort(sortAsc)
            console.log(listTemp)
        })
        .catch((error) => { console.log(error) })
})